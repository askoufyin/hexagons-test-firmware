#!/usr/bin/env python
#
import os, sys
import struct


class hexfile:
	_file = ""
	_data = bytearray()
	
	def __init__(self, name):
		self._file = name
	
	def parse(self):
		with open(self._file) as f:
			lines = f.readlines()
			
		lines = [x.strip() for x in lines]
		
		# two config words
		_data = bytearray.fromhex("0000 0000")
		_memd = bytearray(0x8000);
		_last = 0
		
		lba = 0
		
		for line in lines:
			if len(line) == 0:
				continue
			
			# check Start-Of-Record marker
			if line[0] != ':':
				continue
			
			temp = bytearray.fromhex(line[1:])
			
			rec_len  = temp[0]
			addr 	 = (temp[1] << 8) | temp[2]
			rec_type = temp[3]
			
			if rec_type == 0x00:
				# 0x00 = DATA
				if rec_len % 2 != 0:
					print("Odd record length")
					break

				if lba >= 0x10000:
					print(temp[4:-1])
				else:
					addr += lba
					for a in range(0, rec_len):
						_memd[addr+a] = temp[a+4]
					last = addr + rec_len
						
			elif rec_type == 0x01:
				# EOF
				break
			
			elif rec_type == 0x02:
				# Extended segment address
				pass
			
			elif rec_type == 0x03:
				# Start segment address
				pass
			
			elif rec_type == 0x04:
				# Extended linear address
				lba = ((temp[4] << 8) | temp[5]) << 16
				#print("LBA=%04X" % lba)
			
			elif rec_type == 0x05:
				# Start linear address
				pass

			else:
				break
		# end for

		_data += _memd[:last]
		
		return _data
	
def main(args):
	sf = args[0]
	hf = hexfile(sf)
	df = os.path.splitext(sf)[0]+'.bin'
	
	bindata = hf.parse()
	#print(bindata)
	out = open(df, "wb")
	out.write(bindata)
	out.close()

if __name__ == '__main__':
	main(sys.argv[1:])